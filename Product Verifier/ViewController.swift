//
//  QRScannerViewController.swift
//  QRCodeApp
//
//  Created by Farukh IQBAL on 21/12/2020.
//

import UIKit
import AVFoundation
import Vision
import PKHUD

class ViewController: UIViewController {
    
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    private var maskLayer = CAShapeLayer()
    let backgroundAlpha: CGFloat = 0.6
    lazy var scanView = ScanView(frame: CGRect(x: view.center.x - 75, y: view.center.y - 150, width: 150, height: 300))
    lazy var topScanView = ScanView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: (view.bounds.height - 300) / 2))
    lazy var bottomScanView = UIView(frame: CGRect(x: 0, y: view.bounds.height - (view.bounds.height - 300) / 2, width: view.bounds.width, height: (view.bounds.height - 300) / 2))
    lazy var leftScanView = ScanView(frame: CGRect(x: 0, y: view.bounds.height - (view.bounds.height - 300) / 2 - 300, width: (view.bounds.width - 150) / 2, height: 300))
    lazy var rightScannView = ScanView(frame: CGRect(x: view.center.x + 75, y: view.bounds.height - (view.bounds.height - 300) / 2 - 300, width: (view.bounds.width - 150) / 2, height: 300))
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
    
    lazy var fullScanView = ScanView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: (view.bounds.height)))
    
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        
        let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
        colorAnimation.fromValue = UIColor.systemGray4.cgColor
        colorAnimation.duration = 0.25
        sender.layer.add(colorAnimation, forKey: "ColorPulse")
        
        DispatchQueue.main.async {
            HUD.show(.labeledProgress(title: "Wait a moment", subtitle: "We are uploading your image"))
            HUD.hide(afterDelay: 1) { success in
                
            }
        }
    }
    func addNavBarImage() {
        let navController = navigationController!
        let image = UIImage(named: "Logo") //Your logo url here
        let imageView = UIImageView(image: image)
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
    }
    
    func setTitle(_ title: String, andImage image: UIImage) {
            let titleLbl = UILabel()
            titleLbl.text = title
            titleLbl.textColor = UIColor.white
            titleLbl.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
            let imageView = UIImageView(image: image)
            let titleView = UIStackView(arrangedSubviews: [imageView, titleLbl])
            titleView.axis = .horizontal
            titleView.spacing = 10.0
            navigationItem.titleView = titleView
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Product Verifier"
        
        topScanView.isCentered = false
        // bottomScanView.isCentered = false
        rightScannView.isCentered = false
        leftScanView.isCentered = false
        scanView.isCentered = true
        
        let x = (view.bounds.width - 150) / 2
        let y = (view.bounds.height - 300) / 2

        let w = 150.0
        let h = 300.0
        
        fullScanView.makeClearHole(rect: CGRect(x: x, y: y, width: w, height: h), radius: 24)
        
        let button = UIButton(frame: CGRect(x: bottomScanView.frame.width / 2 - 100, y: bottomScanView.frame.height / 2, width: 200, height: 48))
        button.backgroundColor = UIColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 24
        button.setTitle("FOTO AUFNEHMEN", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)

        bottomScanView.addSubview(button)
        bottomScanView.backgroundColor = .clear
        
        let buttonCircle = UIButton(frame: CGRect(x: bottomScanView.frame.width / 2 + 100 + 12, y: bottomScanView.frame.height / 2, width: 48, height: 48))
        buttonCircle.backgroundColor = UIColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        buttonCircle.layer.cornerRadius = 24
        buttonCircle.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        buttonCircle.setImage(UIImage(named: "BoltIcon"), for: .normal)
        bottomScanView.addSubview(buttonCircle)
        
        let buttonInfoIcon = UIBarButtonItem(title: nil, image: UIImage(named: "InfoIcon"), primaryAction: nil, menu: nil)
        buttonInfoIcon.tintColor = .white
        navigationItem.rightBarButtonItem = buttonInfoIcon
        
        // let logo = UIImage(named: "Logo")
        // let imageView = UIImageView(image:logo)
        // let button3 = UIBarButtonItem(title: nil, image: logo, primaryAction: nil, menu: nil)
        // button3.tintColor = nil
        // self.navigationItem.leftBarButtonItems = [button3]
        
        // setTitle("Product Verifier", andImage: logo!)
        
        // addNavBarImage()
        
        // setTitle("Product Verifier", andImage: UIImage(named: "AppIcon")!)
        
        // Get the back-facing camera for capturing videos
        guard let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            //            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.backgroundColor = .black
            view.layer.addSublayer(videoPreviewLayer!)
            view.addSubview(scanView)
           // view.addSubview(topScanView)
           // view.addSubview(leftScanView)
           // view.addSubview(rightScannView)
            view.addSubview(fullScanView)
            view.addSubview(bottomScanView)
            videoPreviewLayer?.backgroundColor = CGColor(gray: 1, alpha: 0.6)
            // Start video capture
            captureSession.startRunning()
            
            // Move the message label and top bar to the front
            
            // Initialize QR Code Frame to highlight the QR Code
            qrCodeFrameView = UIView()
            
            if let qrcodeFrameView = qrCodeFrameView {
                qrcodeFrameView.layer.borderColor = UIColor.yellow.cgColor
                qrcodeFrameView.layer.borderWidth = 4
                view.addSubview(qrcodeFrameView)
                view.bringSubviewToFront(qrcodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue anymore
            print(error)
            return
        }
    }
    
    func removeMask() {
        maskLayer.removeFromSuperlayer()
    }
    
    private func detectRectangle(in image: CVPixelBuffer) {
        let request = VNDetectRectanglesRequest(completionHandler: { (request: VNRequest, error: Error?) in
            DispatchQueue.main.async {
                
                guard let results = request.results as? [VNRectangleObservation] else { return }
                self.removeMask()
                
                guard let rect = results.first else{return}
                self.drawBoundingBox(rect: rect)
            }
        })
        
        //Set the value for the detected rectangle
        request.minimumAspectRatio = VNAspectRatio(0.3)
        request.maximumAspectRatio = VNAspectRatio(0.9)
        request.minimumSize = Float(0.3)
        request.maximumObservations = 1
        
        
        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: image, options: [:])
        try? imageRequestHandler.perform([request])
    }
    
    func imageExtraction(_ observation: VNRectangleObservation, from buffer: CVImageBuffer) -> UIImage {
        var ciImage = CIImage(cvImageBuffer: buffer)
        
        let topLeft = observation.topLeft.scaled(to: ciImage.extent.size)
        let topRight = observation.topRight.scaled(to: ciImage.extent.size)
        let bottomLeft = observation.bottomLeft.scaled(to: ciImage.extent.size)
        let bottomRight = observation.bottomRight.scaled(to: ciImage.extent.size)
        
        // pass filters to extract/rectify the image
        ciImage = ciImage.applyingFilter("CIPerspectiveCorrection", parameters: [
            "inputTopLeft": CIVector(cgPoint: topLeft),
            "inputTopRight": CIVector(cgPoint: topRight),
            "inputBottomLeft": CIVector(cgPoint: bottomLeft),
            "inputBottomRight": CIVector(cgPoint: bottomRight),
        ])
        
        let context = CIContext()
        let cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        let output = UIImage(cgImage: cgImage!)
        
        //return image
        return output
    }
    
    func drawBoundingBox(rect : VNRectangleObservation) {
        let transform = CGAffineTransform(scaleX: 1, y: -1)
            .translatedBy(x: 0,
                          y: -self.videoPreviewLayer!.bounds.height)
        let scale = CGAffineTransform.identity
            .scaledBy(x: self.videoPreviewLayer!.bounds.width,
                      y:self.videoPreviewLayer!.bounds.height)
        let bounds = rect.boundingBox
            .applying(scale).applying(transform)
        createLayer(in: bounds)
    }
    private var bBoxLayer = CAShapeLayer()
    private func createLayer(in rect: CGRect) {
        bBoxLayer = CAShapeLayer()
        bBoxLayer.frame = rect
        bBoxLayer.cornerRadius = 10
        bBoxLayer.opacity = 1
        bBoxLayer.borderColor = UIColor.systemBlue.cgColor
        bBoxLayer.borderWidth = 6.0
        videoPreviewLayer!.insertSublayer(maskLayer, at: 1)
    }
    func removeBoundingBoxLayer() {
        bBoxLayer.removeFromSuperlayer()
    }
    
}

extension UIView {
    func makeClearHole(rect: CGRect, radius: Int) {
        let maskLayer = CAShapeLayer()
        maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        maskLayer.fillColor = UIColor.black.cgColor
        
        let pathToOverlay = UIBezierPath(rect: self.bounds)
        pathToOverlay.append(UIBezierPath(roundedRect: rect, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: radius, height: radius)))
        pathToOverlay.usesEvenOddFillRule = true
        maskLayer.path = pathToOverlay.cgPath
        
        self.layer.mask = maskLayer
    }
}

extension ViewController: AVCaptureMetadataOutputObjectsDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let frame = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            debugPrint("unable to get image from sample buffer")
            return
        }
        self.detectRectangle(in: frame)
        
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        // Get the metadata object
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
            }
        }
    }
}

extension CGPoint {
    func scaled(to size: CGSize) -> CGPoint {
        return CGPoint(x: self.x * size.width,
                       y: self.y * size.height)
    }
}
