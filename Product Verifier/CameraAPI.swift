//
//  CameraChecker.swift
//  NT Alarm
//
//  Created by Кирилл Стрельцов on 14.06.21.
//

import UIKit
import AVFoundation

class CameraAPI {
    static func isAllowedToUseCamera() -> Bool {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
            
        switch cameraAuthorizationStatus {
        case .denied: return false
        case .authorized: return true
        case .restricted: break
        default:
            return false
        }
        return false
    }
    
    static func noCameraRightsAlert() -> UIAlertController {
        let alert = UIAlertController(title: "No camera access", message: "Unfortunately you've denied camera access to the app. You have to manually navigate to Settings and give the app permission to use camera", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let navigateToSettings = UIAlertAction(title: "Navigate to Settings", style: .default) { _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }
        alert.addAction(cancel)
        alert.addAction(navigateToSettings)
        return alert
    }
}
