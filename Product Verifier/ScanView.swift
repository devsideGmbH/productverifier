import UIKit

public class ScanView: UIView {
    
    public var screenWidth: CGFloat = UIScreen.main.bounds.width < UIScreen.main.bounds.height ? UIScreen.main.bounds.width : UIScreen.main.bounds.height
    public var screenHeight: CGFloat = UIScreen.main.bounds.width < UIScreen.main.bounds.height ? UIScreen.main.bounds.height : UIScreen.main.bounds.width
    
    public lazy var borderColor = UIColor.white
    public lazy var borderLineWidth: CGFloat = 0.2
    public lazy var cornerColor = UIColor.red
    public lazy var cornerWidth: CGFloat = 2.0
    public lazy var backgroundAlpha: CGFloat = 0.6
    public lazy var scanBorderWidthRadio: CGFloat = 0.6
    lazy var scanBorderWidth = scanBorderWidthRadio * screenWidth
    lazy var scanBorderHeight = scanBorderWidth
    
    lazy var scanBorderX: CGFloat = getXY().0
    lazy var scanBorderY: CGFloat = getXY().1
    
    var isCentered = false
        
    lazy var contentView = UIView(frame: CGRect(x: scanBorderX, y: scanBorderY, width: scanBorderWidth, height:scanBorderHeight))
    
    override public init(frame: CGRect) {
        
        super.init(frame: frame)
        backgroundColor = .clear
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func getXY() -> (CGFloat, CGFloat) {
        var x: CGFloat = 0
        var y: CGFloat = 0
        let smaller = screenWidth <= screenHeight ? screenWidth : screenHeight
        let larger = screenWidth > screenHeight ? screenWidth : screenHeight
        let interfaceOrientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation
        if interfaceOrientation == .landscapeLeft || interfaceOrientation == .landscapeRight {
            x = 0.85 * (1 - scanBorderWidthRadio) * larger
            y = 0.3 * (smaller - scanBorderWidth)
        } else if interfaceOrientation == .portrait {
            x = 0.5 * (1 - scanBorderWidthRadio) * smaller
            y = 0.35 * (larger - scanBorderWidth)
        }
        return (x, y)
    }
    
    override public func draw(_ rect: CGRect) {
        
        scanBorderX = getXY().0
        scanBorderY = getXY().1
        
        super.draw(rect)
        
        drawScan(rect)
        
        contentView.backgroundColor = .clear
        contentView.clipsToBounds = true
        addSubview(contentView)
    }
}



// MARK: - CustomMethod
extension ScanView {
    
    func drawScan(_ rect: CGRect) {
        if !isCentered {
            UIColor.black.withAlphaComponent(backgroundAlpha).setFill()
            UIRectFill(rect)
        }
    }
}
